package com.bikini.spider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import com.bikini.common.pojo.Product;
import com.bikini.common.utils.JdUrlUtil;


@Component
public class PageProcessor {

	public List<Product> getProductList() {

		List<Product> jdProductList = new ArrayList<Product>();
		try {
			for (int i = 2; i <= 90; i++) {
				// 获取url
				String url = JdUrlUtil.getUrl("手机", i);
				Document doc = Jsoup.connect(url).maxBodySize(0).timeout(5000).get();
				// doc获取整个页面的所有数据
				Elements ulList = doc.select("ul[class='gl-warp clearfix']");
				Elements liList = ulList.select("li[class='gl-item']");
				// 匹配pId
				String regEx = "[^0-9]";
				Pattern pattern = Pattern.compile(regEx);
				// 循环liList的数据
				for (Element item : liList) {
					Product jdProduct = new Product();
					// 排除广告位置
					if (!item.select("span[class='p-promo-flag']").text().trim().equals("广告")) {
						// 商品名称
						String pName = item.select("div[class='p-name p-name-type-2']").select("em").text();
						// System.out.println(pName);
						// 单个商品地址
						String pAddr = item.select("div[class='p-name p-name-type-2']").select("a[href]").attr("href");
						// System.out.println(pAddr);
						// 商品Id
						String pId = pattern.matcher(pAddr).replaceAll("").trim();
						// System.out.println(pId);
						// 商品价格
						String pPrice = item.select("div[class='p-price']").select("i").text();
						// System.out.println(pPrice);
						// 手机图片地址
						String pImg = item.select("div[class='p-img']").select("a[href]").select("img")
								.attr("source-data-lazy-img");
						// System.out.println(pImg);
						// 店铺地址
						String pShopAddr = item.select("div[class='p-shop']").select("span").select("a").attr("href");
						// System.out.println(pShopAddr);
						// 店铺名称
						String pShop = item.select("div[class='p-shop']").select("a[href]").text();
						// System.out.println(pShop);
						// System.out.println(item.select("div[class='p-commit']").select("a").select("strong").select("a").text());//评论数量
						jdProduct.setpId(pId);
						jdProduct.setpName(pName);
						jdProduct.setpAddr(pAddr);
						jdProduct.setpPrice(pPrice);
						jdProduct.setpImg(pImg);
						jdProduct.setpShopAddr(pShopAddr);
						jdProduct.setpShop(pShop);
					}
					jdProductList.add(jdProduct);
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jdProductList;
	}
}
