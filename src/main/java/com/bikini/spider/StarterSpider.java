package com.bikini.spider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.bikini.spider")
public class StarterSpider {
	public static void main(String[] args) {
		SpringApplication.run(StarterSpider.class, args);  
	}
}
 