package com.bikini.spider.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.bikini.common.pojo.Product;

@Mapper
public interface SpiderMapper {
	void productSave(Product product);
}
