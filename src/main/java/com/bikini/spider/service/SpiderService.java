package com.bikini.spider.service;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bikini.common.pojo.Product;
import com.bikini.spider.PageProcessor;
import com.bikini.spider.mapper.SpiderMapper;

@Service
public class SpiderService {
	@Autowired
	private SpiderMapper spiderMapper;
	@Autowired
	private PageProcessor pageProcessor;

	public void getProduct() {

		List<Product> productList = pageProcessor.getProductList();
		Iterator<Product> jdProductList = productList.iterator();
		while(jdProductList.hasNext()) {
			spiderMapper.productSave(jdProductList.next());
		}
	}
}
